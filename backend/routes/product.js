import express from 'express'
import { Product, Bid, User } from '../orm/index.js'
import authMiddleware from '../middlewares/auth.js'
import { getDetails } from '../validators/index.js'

const router = express.Router()

router.get('/api/products', async (req, res, next) => {
  const result = await Product.findAll();
  res.status(200).send(result)
})

router.get('/api/products/:productId', async (req, res) => {
  const result = await User.findOne({ where: { id: req.params.productId }});
  res.status(200).send(result)
})

// You can use the authMiddleware to authenticate your endpoint ;)

router.post('/api/products', (req, res) => {
  const addProduct = Product.create({
   name: req.body.name,
   description: req.body.description,
   category : req.body.category,
   originalPrice : req.body.originalPrice,
   pictureUrl : req.body.pictureUrl,
   endDate : req.body.endDate
  })
  res.status(200).send(addProduct)
})

router.put('/api/products/:productId', async (req, res) => {
  res.status(600).send()
})

router.delete('/api/products/:productId', async (req, res) => {
  res.status(600).send()
})

export default router
