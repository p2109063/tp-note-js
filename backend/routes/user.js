import express from 'express'
import { User, Product, Bid } from '../orm/index.js'

const router = express.Router()

router.get('/api/users/:userId', async (req, res) => {
  const result = await User.findOne({ where: { id: req.params.userId }});
  res.status(200).send(result)
})

export default router
